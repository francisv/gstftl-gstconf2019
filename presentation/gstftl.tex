%% - Talk at GstConf2019
%% - Talk length is 20min
%% - Style is ornate

\usepackage[english]{babel}
\usepackage{caption}
\usepackage{times}
\usepackage{listings}
\usepackage{courier}
\usepackage{tabularx}
% \usepackage{array}
% \usepackage[T1]{fontenc}
\usepackage[final]{microtype}
% Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.
\graphicspath{{../src/img/}}
% \emergencystretch=1em

\title[\textbf{\texttt{GstFTL}}]
{GStreamer and the Faster Than Light (FTL) streaming protocol}

\subtitle
{\textbf{\texttt{GstFTL}}}

\author[Velázquez, Steffens] % (optional, use only with lots of authors)
{Francisco Javier Velázquez-García \and Jan Steffens}

\institute[Make.TV]
{
  Make.TV\\
  A Division of LTN Global
}

\date[GstConf 2019]
{GStreamer Conference, 2019}

% \AtBeginSubsection[]
% {
% \begin{frame}<beamer>{Outline}
%   \tableofcontents
% \end{frame}
% }

%   \beamerdefaultoverlayspecification{<+->}
\lstset{
  basicstyle=\ttfamily,
  captionpos=b,
  frame=single,
  alsodigit=-,
  alsoletter=/,
  classoffset=0,
  morekeywords={videosink,video/x-h264,byte-stream,audiosink,audio,x-opus,au},
  keywordstyle=\color{red},
  classoffset=1,
  classoffset=0,
}

\begin{document}

\maketitle

\begin{abstract}
  FTL is a streaming protocol that allows sub-second latency streaming
  of video and audio to \texttt{mixer.com}.  FTL stands for Faster
  Than Light streaming protocol and is part of the Mixer video game
  live streaming platform owned by Microsoft.  We have implemented the
  \texttt{ftlsink} GStreamer plugin to stream to the Mixer streaming
  service.  The plugin is implemented using the SDK of Mixer and it
  works out-of-the-box when building GStreamer pipelines to stream
  H.264 video and Opus audio formats.  Results from our evaluation
  show that the plugin performs as efficient as using the client
  included in the SDK.
\end{abstract}

\only<article>{
  \tableofcontents
  \listoftables
  \listoffigures
  \lstlistoflistings
  \newpage
}

\begin{frame}
  \titlepage
  \note<1>[item]{Explain FTL}
  \note<1>[item]{Demo}
\end{frame}

%% - Exactly two or three sections (other than the summary).
%%
%% - At *most* three subsections per section.
%%
%% - Talk about 30s to 2min per frame.  So there should be between
%% about 15 and 30 frames, all told.
%%
%% - If I omit details that are vital to the proof/implementation, I
%% just say so once.  Everybody will be happy with that.

\section{Motivation}

\subsection{Mixer}

\begin{frame}{The Mixer Live Streaming Service}
  \begin{figure}[ht!]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{../src/img/mixer.pdf_tex}
    % \caption{Mixer Logo}
    \label{fig:mixer}
  \end{figure}
  \begin{table}[ht!]
    \centering
    \setkeys{Gin}{width=\linewidth}
    \begin{tabularx}{\textwidth}{XXXXXX}
      \includegraphics{MakeTV_logo-badge-RGB-CLR.png} & \includegraphics{kevin-standard.png} & \def\svgwidth{\linewidth} \input{../src/img/OBS.pdf_tex} & \includegraphics{Windows_10_logo-cropped.png} & \includegraphics{xboxone-cropped.png}  & \includegraphics{XSplit.png}
      \\
    \end{tabularx}
    \caption*{Applications that stream to Mixer}
    \label{tab:ftl-apps}
  \end{table}

  \note<1>[item]{Video game live streaming platform owned by
    Microsoft}

  \note<1>[item]{Enables streamers to interact with their viewers in
    real-time}

  \note<1>[item]{Co-streaming up to 4 streamers}

  \note<1>[item]{Specifically tailored towards streams that engage
    viewers with interactive controls.}

  \note<1>[item]{Launched on 2016 as \textit{Beam}, but renamed
    \textit{Mixer} in 2017}

  \note<1>[item]{69,000 streamers by Q4'18}

  \note<1>[item]{Emphasizes \alert{low stream latency}}
\end{frame}

Mixer is a video game live streaming platform owned by Microsoft.  It
was launched on 2016 as \textit{Beam}, but was renamed as
\textit{Mixer} in 2017~\cite{contributors19:_mixer}.

It enables streamers to interact with their viewers in real-time and
is specifically tailored towards streams that engage viewers with
interactive controls \cite{community19:_ftl_faster_than_light_forum}.
It allows up to four streamers to broadcast on one page in a
\textit{split-scree} view with a shared chat.

Mixer had 69,000 streamers by Q4'18
\cite{may19:_live_stream_q418_repor_first} and it emphasizes
\textit{low stream latency} and interactivity.  Mixer provides a
platform for allowing viewers to perform actions that can influence a
stream, such as voting or influencing game play.  It offers
monetization programs.

The \href{https://www.livevideocloud.com/}{Live Video Cloud of
  Make.TV} enables users to stream to Mixer.  Other applications
include to stream to Mixer are \cite{mixer:_ftl_faster_than_light}:

\begin{itemize}
\item Make.TV
\item Streamlabs
\item OBS
\item Windows 10
\item Xbox One
\item XSplit
\end{itemize}

\begin{frame}<beamer>[fragile]
  \frametitle{Increasing Trend of Mixer's Usage}
  \begin{table}[ht!]
    \centering
    \renewcommand{\tabularxcolumn}[1]{>{\small}m{#1}}
    \begin{tabularx}{1.0\linewidth}[ht]{X>{\centering\arraybackslash}X}
      \includegraphics[width=0.45\columnwidth]{q3_2019_Mixer-Total-Hours-Watched-Since-Q1-18-cropped.png} & \includegraphics[width=0.45\columnwidth]{q3_2019_Mixer-Total-Hours-Streamed-Since-Q1-18-cropped.png} \\
      \includegraphics[width=0.45\columnwidth]{q3_2019_Mixer-Unique-Channels-Since-Q1-18-cropped.png}     & Source: Newzoo.com
    \end{tabularx}
    \label{tab:trend}
  \end{table}
  \note<1>[item]{Total \alert{hours watched} increased more than double in the last year}
  \note<1>[item]{Total \alert{hours streamed} increased almost 3 times since last quarter}
  \note<1>[item]{\alert{Channels} doubled between Q2 and Q3}
  \note<1>[item]{More metrics at \texttt{newzoo.com}}
\end{frame}

\begin{figure}[ht!]
  \centering
  \includegraphics[width=\columnwidth]{q3_2019_Mixer-Total-Hours-Watched-Since-Q1-18-cropped.png}
  \caption{Mixer Total Hours Watched Since QT'18~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}}
  \label{fig:mixer-hrs-watched}
\end{figure}

\begin{figure}[ht!]
  \centering
  \includegraphics[width=\columnwidth]{q3_2019_Mixer-Total-Hours-Streamed-Since-Q1-18-cropped.png}
  \caption{Mixer Total Hours Streamed Since QT'18~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}}
  \label{fig:mixer-hrs-streamed}
\end{figure}

\begin{figure}[ht!]
  \centering
  \includegraphics[width=\columnwidth]{q3_2019_Mixer-Unique-Channels-Since-Q1-18-cropped.png}
  \caption{Mixer Unique Channels Since QT'18~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}}
  \label{fig:mixer-channels}
\end{figure}

\Cref{fig:mixer-hrs-watched} shows that the total amount of hours
watched has increased more than double since the Q1 of 2018.  All this
despite the decrease from last
quarter~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}.
\Cref{fig:mixer-hrs-streamed} shows an increase of 188\% of total
gaming hours streamed last quarter.  It accounts for almost three
times more~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}.
\Cref{fig:mixer-channels} shows how the unique channels in Mixer
doubled between Q2 and Q3.  This and more metrics are available
at~\cite{elliott19:_stream_newzoo_q3_game_stream_repor}.

\subsection{FTL}

\begin{frame}{What is FTL?}
  \begin{figure}[ht!]
    \centering
    \includegraphics[width=0.6\columnwidth]{low-latency.png}
    \caption{Low latency option in Mixer player}
    \label{fig:low-latency}
  \end{figure}
  \note<1>[item]{Competitor of RTMP}
  \note<1>[item]{1080p at 60 fps}
  \note<1>[item]{10 Mbps}
  \note<1>[item]{Interactive}
  \note<1>[item]{SDK, library provide metrics}
  \note<1>[item]{Apparently \alert{VP8} and \alert{AAC} also supported}
  \note<1>[item]{H.264 version \alert{(05/2003)}}
  \note<1>[item]{UDP}
  \note<1>[item]{MIT license}
\end{frame}

FTL is a direct competitor of the Real-Time Messaging Protocol (RTMP).
FTL has a sub-second delay, which contrasts with the delay between 2
and 30 seconds of typical RTMP streams.  Thus, FTL enables streamers
and viewers to communicate close to real-time.

FTL aims at streams to Mixer at 1080p video at 60 fps, and Mixer
supports a bitrate up to 10 Mbps~\cite{mixer19:_stream_settin}.  FTL
uses UDP as transport protocol.  The cog wheel in the menu of the
player of Mixer has an option to toggle \textit{Low Latency}
streaming.  This option is helpful when the Internet connection speed
is not fast enough and the jitter is intolerable.

The FTL-SDK allows developers to create interactive applications that
make possible for streamers to involve viewers in real time while
playing.  For example, let the viewers choose the weapon used.  The
SDK implements H.264 (05/2003)~\cite{itu-t05:_h} video coding, and
provide functions to get metrics. The SDK has a MIT License.

% FTL transcodes are only available to partners.  Partner streamers can
% transcode to 720p, 480p, and 240p.  Stream source must be 1080p or
% lower to receive transcodes.  If the source is higher than 1080p
% transcodes will not be
% generated.  %% https://watchbeam.zendesk.com/hc/en-us/articles/214065923-Transcoding

\section{GstFTL}

\subsection{Basic Ideas for Implementation}

\begin{frame}[fragile]{Our Implementation}
\begin{lstlisting}[label={ftl-capabilities},caption=Capabilities of \texttt{ftlsink}]
  SINK template: 'videosink'
    Availability: Always
    Capabilities:
      video/x-h264
          stream-format: byte-stream
              alignment: au

  SINK template: 'audiosink'
    Availability: Always
    Capabilities:
      audio/x-opus
\end{lstlisting}
  \note<1>[item]{Library \alert<1>{\texttt{gstftl}} compiled with Meson}
  \note<1>[item]{Plugin \alert<1>{\texttt{ftl}} with element \alert<1>{\texttt{ftlsink}}}
  \note<1>[item]{\texttt{stream-key} property}
\end{frame}

We have implemented the library \texttt{gstftl}.  It is compiled with
Meson.  The library contains the plugin \texttt{ftl} with the element
\texttt{ftlsink}.  The element will be soon available at
\href{https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/tree/master/ext/ftl}{gst-plugins-bad/ext/ftl}.

\begin{frame}
  \frametitle{Pipeline}
  \begin{figure}[ht!]
    \centering
    \includegraphics[width=\columnwidth]{pipeline-edited.png}
    \caption{\texttt{ftlsink} in pipeline}
    \label{fig:pipeline}
  \end{figure}
  \note<1>[item]{\texttt{ftlsink} is a \alert{bin} with audio and video elements.}
\end{frame}

\Cref{fig:pipeline} shows that \texttt{ftlsink} is a bin that contains
two elements, one for video and one for audio processing.

\begin{frame}
  \frametitle{Challenges}
  \begin{itemize}
  \item Parsing of Network Abstraction Layer (NAL) units
  \item Locking: video and audio must be ready to send
  \item No \texttt{pkg-config} configuration in \texttt{ftl-sdk}
  \end{itemize}

  \note<1>[item]{Possible bug in \alert{\texttt{h264parse}},
    \texttt{ftlsink} includes a \alert{\emph{au}} parser}

  \note<1>[item]{\texttt{ftlsink} enforces video and audio by
    implementing locking between \texttt{media\_send\_audio} and
    \texttt{media\_send\_video}}

  \note<1>[item]{\texttt{ftl-sdk} moved from Github to \alert{Azure
      DevOps} git repository}

\end{frame}

The H.264 encoder packets video data in \emph{Network Abstraction
  Layer (NAL)} units.  In GStreamer, the element \texttt{h264parse}
would normally parse the byte stream by NAL units and provide one NAL
unit per buffer.  However, we encountered a problem when taking the
output from \texttt{h264parse}, probably caused by a bug in
\texttt{h264parse}.  Therefore, we decided that \texttt{ftlsink}
should require H264 video with \emph{access unit (au)} alignment.
This alignment contains a set of NAL units with data for exactly one
complete decoded picture.  To do the parsing of access units, we
included the function \texttt{get\_next\_nalu} in
\texttt{gstftlvideosink.c}, which is based on the function
\texttt{h264\_get\_nalu} from \texttt{file\_parser.c} from the FTL-SDK.

\texttt{media\_send\_audio} and \texttt{media\_send\_video} require
locking because video cannot be sent before the audio stream is also
ready.  At the same time, audio should not be sent before video is
also sending.  \texttt{ftlsink} requires both, video and audio, to
work.

The FTL library provided by Mixer does not contain information for
building \texttt{ftlsink} with \texttt{pkgconf}.  We have added the
\texttt{.pc} file containing the needed information in our repository
\href{https://github.com/maketv/ftl-sdk}{https://github.com/maketv/ftl-sdk}.

\subsection{Evaluation}

\begin{frame}[fragile]{Testing with \texttt{gst-launch}}
\begin{lstlisting}[language=bash,caption=Command to test \texttt{ftlsink},label={lst:gstlaunchtest},alsodigit={-,.},emph={f.videosink,f.audiosink,stream-key,ftlsink},emphstyle={\color{red}}]
gst-launch-1.0 filesrc location=videotestsrc.h264 !\
h264parse ! f.videosink \
filesrc location=sine.ogg ! oggdemux ! opusparse ! \
f.audiosink \
ftlsink name=f \
stream-key=<your-mixer-key>
\end{lstlisting}
  \note<1>[item]{\alert{\texttt{/usr/bin/time}} for CPU usage}
  \note<1>[item]{\alert{\texttt{videotestsrc.h264}}: zero B frames}
  \note<1>[item]{\alert{bframes}: Number of B-frames between I and P}
  \note<1>[item]{\alert{b-adapt}: Automatically decide how many B-frames to use}
  \note<1>[item]{\alert{key-int-max}: Maximal distance between two key-frames (0 for automatic)}
  \note<1>[item]{\alert{speed-preset}: Preset name for speed/quality trade off options (can affect decode compatibility impose restrictions separately for your target decoder)}
  \note<1>[item]{\alert{tune}: Preset name for non-psychovisual tuning options}
\end{frame}

Listing \ref{lst:gstlaunchtest} shows the \texttt{gst-launch} command
to test \texttt{ftlsink}.  Listing \ref{lst:h264file} shows the
command to create the file we used to do the evaluation.
Listing \ref{lst:opusfile}

All commands were run in a Dell Laptop model XPS 15 9570 with a 4.1
GHz Intel Core processor and 16 GB SDRAM.  The laptop had access to
internet through its wired interface at a speed that was multiple
times faster than the streamed bitrate.  The metrics are obtained from
the functions provided by the FTL SDK, and the CPU utilization is
obtained by using the \texttt{time} linux command and the printing
format string \texttt{\%P}.  The results of the evaluation are
presented in \Cref{tab:stats}.

\begin{lstlisting}[language=bash,caption=Command to create the video test file in H.264 format,label={lst:h264file},alsodigit={-,.},emph={f.videosink,f.audiosink,stream-key,ftlsink},emphstyle={\color{red}}]
gst-launch-1.0 -v videotestsrc num-buffers=1000 !\
timeoverlay !\
video/x-raw,width=1920,height=1080,frame-rate=30/1 !\
x264enc bframes=0 b-adapt=0 key-int-max=30 !\
video/x-h264,profile=constrained-baseline,\
stream-format=byte-stream,\
speed-preset=superfast,tune=zerolatency,bitrate=2800 !\
filesink location=no-b-frame-30fps.h264
\end{lstlisting}

\begin{lstlisting}[language=bash,caption=Command to create the audio test file in Opus format,label={lst:opusfile},alsodigit={-,.},emph={f.videosink,f.audiosink,stream-key,ftlsink},emphstyle={\color{red}}]
gst-launch-1.0 -v audiotestsrc wave=sine num-buffers=1000\
! audioconvert ! opusenc ! oggmux !\
filesink location=sine.ogg
\end{lstlisting}

\begin{frame}
  \frametitle{Results}
  \begin{table}
    \centering
    \begin{tabularx}{1.0\linewidth}{ccccc}
      \textbf{Application} & \textbf{Mean sent} & \textbf{Mean sent} & \textbf{Max. frame}   & \textbf{CPU}         \\
                           & \textbf{(FPS)}     & \textbf{(Mbps)}    & \textbf{size (Mbits)} & \textbf{Utilization} \\
      \hline
      \texttt{ftlsink}     & 30.00              & 2.09               & 0.37                  & \alert{3\%}          \\
      \texttt{ftl\_app}    & 30.00              & 2.11               & 0.37                  & 6\%
    \end{tabularx}
    \caption{Comparison metrics between \texttt{ftlsink} and \texttt{ftl\_app}}
    \label{tab:stats}
  \end{table}
  \note<1>[item]{\texttt{ftlsink} uses half of CPU utilization in comparison to \texttt{ftl\_app}.}
\end{frame}

\Cref{tab:stats} shows that \texttt{ftlsink} is able to send video as fast as the application included in the SDK.
The difference of data sent does not represent a difference in the video presentation at the naked eye.
Remarkable is that \texttt{ftlsink} requires half of the CPU utilization in comparison to \texttt{ftl\_app}.

\subsection{Demo}
\label{sec:demo}

\begin{frame}[fragile]{Pipeline for Demo}
\begin{lstlisting}[language=bash,caption=Command to demo \texttt{ftlsink},label={lst:gstlaunchdemo},alsodigit={-,.},emph={f.videosink,f.audiosink,stream-key,ftlsink,tee,xvimagesink},emphstyle={\color{red}}]
gst-launch-1.0 videotestsrc ! timeoverlay ! \
video/x-raw,width=1920,height=1080,framerate=30/1 !\
x264enc bframes=0 b-adapt=0 key-int-max=30 \
speed-preset=superfast tune=zerolatency \
bitrate=2800 ! tee name=t t. ! queue ! h264parse !\
avdec_h264 ! videoconvert ! xvimagesink \
t. ! queue ! f.videosink \
audiotestsrc ! opusenc ! f.audiosink \
ftlsink name=f \
stream-key=<your-mixer-key>
\end{lstlisting}
\end{frame}

\section*{Summary}

\begin{frame}{Summary}
  % Keep the summary *very short*.
  \begin{itemize}
  \item Mixer.com uses FTL for streaming
  \item FTL transports H.264
  \item \texttt{gstftl} allows you to stream to Mixer.com with a \alert{sub-second} latency
  \item Available at
    \href{https://gitlab.freedesktop.org/francisv/gst-plugins-bad/tree/ftl}
    {\texttt{https://gitlab.freedesktop.org/\\francisv/gst-plugins-bad/tree/ftl}}
  \end{itemize}

  \only<beamer>{  \vskip0pt plus.5fill}

  \begin{itemize}
  \item Limitation: \texttt{ftlsink} requires video \textbf{and}
    audio
  \end{itemize}
\end{frame}

\subsection{Questions}

\begin{frame}
  \frametitle{Thank you!  Questions and Answers?}
  \centering
  \includegraphics[width=0.6\linewidth]{mixer-friends.png} \tiny
  Source: Part of official logos from Mixer \only<article>{\cite{mixer:_ftl_faster_than_light}}
  \\
  \tiny Source: \href{https://www.sae.edu/}{sae.edu}
  \includegraphics[width=0.7\linewidth]{cologne.jpg}

  \note<1>[item]{Kindly invite audience to vote for GStreamer Hackfest
    Sprint 2000 in Make.TV offices}

  \note<1>[item]{We are hiring!}
\end{frame}

\appendix
\section<presentation>*{\appendixname}
\subsection<presentation>*{References}

\only<beamer>{\begin{frame}
    \frametitle<presentation>{References}

    \begin{thebibliography}{10}

      %% Look at `beamerbasecompatibility.sty' for icon options
      \beamertemplateonlinebibitems

    \bibitem{microsoft2019} Microsoft
      \newblock FTL SDK
      \newblock {\em \href{https://dev.azure.com/mixer-oss/Mixer/\_git/ftl-sdk/}{https://dev.azure.com/mixer-oss/Mixer/\_git/ftl-sdk/}}, 2019.

      \beamertemplatearticlebibitems

    \bibitem{itu} ITU-T.
      \newblock H.264: Advanced video coding for generic audiovisual services
      \newblock {\em \href{https://www.itu.int/rec/T-REC-H.264-200305-S/en/}{https://www.itu.int/rec/T-REC-H.264-200305-S/en/}}, 2005.

      \beamertemplateonlinebibitems

    \bibitem{itu} Velázquez-Garcia and Steffens
      \newblock GstFTL Presentation and Handouts Git Repository
      \newblock {\em \href{https://https://gitlab.freedesktop.org/francisv/gstftl-gstconf2019}{https://gitlab.freedesktop.org/francisv/gstftl-gstconf2019}}, 2019.

    \end{thebibliography}
  \end{frame}
}
\printbibliography

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.beamer.gstftl"
%%% End:
