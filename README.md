This repository contains the presentation of `gstftl` for GstConf2019.
The presentation is made using the
[Beamer](https://github.com/josephwright/beamer) class of LaTeX.

# How to compile the PDF

1. To create the slides:

```
pdflatex -file-line-error -interaction=nonstopmode presentation/main.beamer.gstftl.tex

pdflatex -file-line-error -interaction=nonstopmode presentation/main.beamer.gstftl.tex
```

Yes, that was two times the `pdflatex` command :-).  If you want to
compile the slides without presenter notes, comment the line

    \setbeameroption{show notes on second screen=right}

in the `main.beamer.gstftl.tex` file.

2. To create the handouts:

```
pdflatex -file-line-error -interaction=nonstopmode presentation/main.article.gstftl.tex

biber presentation/main.article.gstftl

pdflatex -file-line-error -interaction=nonstopmode presentation/main.article.gstftl.tex

pdflatex -file-line-error -interaction=nonstopmode presentation/main.article.gstftl.tex
```

Yes, that was three times the `pdflatex` command exactly in that order, and `biber` in between.

You might need to install some LaTeX packages for everything to
compile correctly in your computer.

# Contribution guidelines

- Open a new issue in the issue tracker system
- Send pull requests or patches in email format

# Who do I talk to?

- Francisco Javier Velázquez-García
- fvelazquez@make.tv
